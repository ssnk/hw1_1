#!/usr/bin/env python
# -*- coding: utf-8 -*-
# log_format ui_short '$remote_addr $remote_user $http_x_real_ip [$time_local] "$request" '
# '$status $body_bytes_sent "$http_referer" '
# '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER"

# '$request_time';

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log"
}


import json
import sys
import logging
import os
import re


logger_config = {
    "level":    logging.INFO,
    "format":   '[%(asctime)s] %(levelname).1s %(message)s',
    "datefmt":  '%Y.%m.%d %H:%M:%S',
    "filename": None
}
logging.basicConfig(**logger_config)

def set_config(size=config["REPORT_SIZE"], report=config["REPORT_DIR"],log=config["LOG_DIR"] ):

    config = {"REPORT_SIZE":size, "REPORT_DIR": report, "LOG_DIR": log}
    logging.info(f"Current config:{config}")
    if "--config" in sys.argv:
        if len(sys.argv) == sys.argv.index("--config") + 1:
            logging.info(f"No value after --config")
            return config
        config_path = sys.argv[sys.argv.index("--config") + 1]
        logging.info(f"start with conf file \"{config_path}\"")
        try:
            with open(config_path, encoding="utf8") as read_file:
                param_from_file = json.load(read_file)
                logging.info(f"config file:{param_from_file}")

                if "filename" in param_from_file:
                    logger_config["filename"] = param_from_file["filename"]
                    param_from_file.pop("filename")

                #param_from_file  = {}

                config1 = param_from_file

        except FileNotFoundError as error:
            logging.error(f"{error.strerror} - {error.filename}")
        except json.decoder.JSONDecodeError as error:
            logging.error(f"json decode error. {error}")
        except Exception as e:
            logging.exception("Exception")
            sys.exit(-1)#?????? спросить закроется ли файл

    return config



def main():


    print (set_config())



    #print (logger_config)
    #print (config)

    d = 0



    with open("1.txt", encoding="utf8") if d == 1 else open("2.txt", encoding="utf8") as read_file:

        #print (read_file.readlines())
        pass


    #print (os.curdir)
    #m = re.findall(r"nginx-access-ui.log-\d{8}.[gz|bz2]{2,3}"," ".join(os.listdir("./reports")))
    #print(m)
    #print(max(m))

    #ss = os.listdir("./reports")
    #print (ss)





    #print (args.config)

if __name__ == "__main__":
    main()